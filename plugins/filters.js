import Vue from 'vue'

const numeral = require("numeral");
import numeralRU from "numeral/locales/ru";
numeral.locale('ru');

Vue.filter("formatNumber", function (value) {
  return numeral(value).format("0,0");
});