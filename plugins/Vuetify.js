import Vue from 'vue';
import Vuetify from 'vuetify';
import '@mdi/font/css/materialdesignicons.css';
import ru from 'vuetify/es5/locale/ru'
Vue.use(Vuetify);


export default ctx => {
  const vuetify = new Vuetify({
    theme: {
      dark: false,
    },
    lang: {
      locales: { ru },
      current: 'ru'
    },
  });

  ctx.app.vuetify = vuetify;
  ctx.$vuetify = vuetify.framework;
}
