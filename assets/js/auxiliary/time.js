/* Получение строки со временем, в формате "A ч. B мин." */
function getTime(time) { // В качестве параметра передаются минуты в формате int, округлённые в меньшую сторону
    let str = "";
    let hours = Math.floor(time / 60); // Преобразовываем минуты в целые часы
    let minutes = time - hours * 60; // Получаем количество минут за вычетом целого часа
    if (hours) {
        str += hours + " ч. ";
    }
    if (minutes) {
        str += minutes + " мин.";
    }
    if (str.length !== 0) {
        return str;
    }
    else {
        return "0 мин."
    }
}

/* Получение строки с разницей во времени между первоначальным и текущим временеи */
function getDelta(initial, current) {
    let str = "";
    let delta = 0;
    if (current < initial) { // Если текущее время меньше первоначального, возвращаем дельту зелёного цвета
        delta = initial - current;
        str = " <span style=\"color:green\"> -" + getTime(delta) + "</span>";
    } else if (current > initial) { // Если текущее время больше первоначального, возвращаем дельту красного цвета
        delta = current - initial;
        str = " <span style=\"color:red\"> +" + getTime(delta) + "</span>";
    }
    return str;
}

/* Получение строки со временем и разницей во времени между первоначальным временем и текущим */
function getTimeStr(i,t) {
    return getTime(t) + getDelta(i,t);
}

export {getTimeStr};