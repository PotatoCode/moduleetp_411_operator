const turf = require('@turf/turf');

function setRoutesInfo() {
    window.routesInfo = {
        straight: document.getElementById("info-A"),
        reverse: document.getElementById("info-B"),
        full: document.getElementById("info-C"),
        vehiclesCount: document.getElementById("info-D"),
        interval: document.getElementById("info-E")
    };
    window.routesLength = {
        vehiclesCount: 0,
        straight: 0,
        reverse: 0,
        full: function() {return this.straight + this.reverse}
    };
    window.routesTime = {
        straight: 0,
        reverse: 0,
        interval: 0,
        full: function() {return this.straight + this.reverse}
    };
    window.routesInitialTime = {
        straight: 0,
        reverse: 0,
        full: function() {return this.straight + this.reverse},
        interval: 0
    };
}
// 19w31d3 old version
// function setDirectionInfo(data, getTimeStr) {
//     setRoutesInfo();
//     setDirectionLength(data, getTimeStr);
// }
// function updateDirectionData(data, direction, getTimeStr) {
//     window.routesLength[direction] = parseFloat(turf.length(turf.lineString(data.geometry.coordinates), {units: "kilometers"}).toFixed(1));
//     window.routesTime[direction] = Math.floor((window.routesLength[direction] / 16.6) * 60);
//     if (!window.routesInitialTime[direction]) {
//         window.routesInitialTime[direction] = window.routesTime[direction];
//     }
//     if (!window.routesLength.vehiclesCount) {
//         window.routesLength.vehiclesCount = data.properties.vehiclesNum;
//         window.routesInfo.vehiclesCount.innerHTML = window.routesLength.vehiclesCount;
//     }
//     window.routesInfo[direction].innerHTML = window.routesLength[direction] + " км, время в пути: " + getTimeStr(window.routesInitialTime[direction], window.routesTime[direction]);
//     if (window.routesTime.straight && window.routesTime.reverse) {
//         window.routesTime.interval = Math.ceil(window.routesTime.full() / window.routesLength.vehiclesCount);
//         if (!window.routesInitialTime.interval) {
//             window.routesInitialTime.interval = window.routesTime.interval;
//         }
//         if (!window.routesInitialTime.full) {
//             window.routesInitialTime.full = window.routesTime.full();
//         }
//         window.routesInfo.interval.innerHTML = getTimeStr(window.routesInitialTime.interval, window.routesTime.interval);
//     }
//     if (window.routesLength.straight && window.routesLength.reverse) {
//         window.routesInfo.full.innerHTML = window.routesLength.full().toFixed(1) + " км, время в пути: " + getTimeStr(window.routesInitialTime.full, window.routesTime.full());
//     }
// }
// function setDirectionLength(data, getTimeStr) {
//     if (!data.properties.dir) {
//         updateDirectionData(data, "straight",getTimeStr);
//     } else {
//         updateDirectionData(data, "reverse",getTimeStr);
//     }
//     updateDirectionData(data, "straight", getTimeStr);

// }
// function updateDirectionInfo(draw,getTimeStr) {
//     let f = draw.getAll();
//     f.features.forEach((data) => {
//         if (data.geometry.type.includes("LineString")) {
//             setDirectionLength(data, getTimeStr)
//         }
//     });
// }


// 19w31d3 new version
function updateTable(getTimeStr) {
    window.routesInfo["straight"].innerHTML = window.routesLength["straight"] + " км, время в пути: " + getTimeStr(window.routesInitialTime["straight"], window.routesTime["straight"]);
    window.routesInfo["reverse"].innerHTML = window.routesLength["reverse"] + " км, время в пути: " + getTimeStr(window.routesInitialTime["reverse"], window.routesTime["reverse"]);
    if (window.routesTime.full()) {
        window.routesTime.interval = Math.ceil(window.routesTime.full() / window.routesLength.vehiclesCount);
        if (!window.routesInitialTime.interval) {
            window.routesInitialTime.interval = window.routesTime.interval;
        }
        // if (!window.routesInitialTime.full) {
        //     window.routesInitialTime.full = window.routesTime.full();
        // }
        window.routesInfo.interval.innerHTML = getTimeStr(window.routesInitialTime.interval, window.routesTime.interval);
    }
    if (window.routesLength.full()) {
        window.routesInfo.full.innerHTML = window.routesLength.full().toFixed(1) + " км, время в пути: " + getTimeStr(window.routesInitialTime.full(), window.routesTime.full());
    }
}

function updateInitialData(data, direction) {
    window.routesLength[direction] = parseFloat(turf.length(turf.multiLineString(data.geometry.coordinates), {units: "kilometers"}).toFixed(1));
    window.routesTime[direction] = Math.floor((window.routesLength[direction] / 16.6) * 60);
    if (!window.routesInitialTime[direction]) {
        window.routesInitialTime[direction] = window.routesTime[direction];
    }
    if (!window.routesLength.vehiclesCount) {
        window.routesLength.vehiclesCount = data.properties.vehiclesNum;
        window.routesInfo.vehiclesCount.innerHTML = window.routesLength.vehiclesCount;
    }
}

function updateData(data, getTimeStr) {
    let direction = "straight"
    if (data.properties.dir === 1) {
        direction = "reverse"
    }
    window.routesLength[direction] = parseFloat(turf.length(turf.lineString(data.geometry.coordinates), {units: "kilometers"}).toFixed(1));
    window.routesTime[direction] = Math.floor((window.routesLength[direction] / 16.6) * 60);
    updateTable(getTimeStr)
}

function setDirectionInfo(data, getTimeStr) {
    setRoutesInfo()
    data.features.forEach((d, i) => {
        let direction = "straight"
        if (d.properties.dir === 1) {
            direction = "reverse"
        }
        updateInitialData(d, direction, getTimeStr)
    })
    updateTable(getTimeStr)
}

function updateDirectionInfo(draw, getTimeStr) {
    let f = draw.getAll();
    f.features.forEach((data) => {
        if (data.geometry.type.includes("LineString")) {
            updateData(data, getTimeStr)
        }
    });
}

export {setDirectionInfo, updateDirectionInfo}
