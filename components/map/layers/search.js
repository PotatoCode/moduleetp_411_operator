import {point} from '@turf/helpers';
import searchIcon from '~/assets/img/marker.svg';

export const search = {
  methods: {
    showSearchResult(coorinates) {
      const features = [point(coorinates)];
      const source = this.map.getSource('source-search');
      if (!source) {
        this.map.addSource('source-search', {
          type: 'geojson',
          data: {
            features,
            type: 'FeatureCollection',
          }
        });
      } else {
        source.setData({
          features,
          type: 'FeatureCollection',
        });
      }
      if (!this.map.getLayer('layer-search')) {
        this.map.addLayer({
          id: 'layer-search',
          type: 'symbol',
          source: 'source-search',
          layout: {
            'icon-image': 'search-icon',
            'icon-size': .5,
            'icon-allow-overlap': true,
            'icon-ignore-placement': true,
            'icon-anchor': 'bottom'
          },
        });
      }
      this.$bus.$emit('fitMap', coorinates, 18);
    },
    hideSearchResult() {
      if (this.map.getLayer('layer-search')) {
        this.map.removeLayer('layer-search')
      }
      if (this.map.getSource('source-search')){
        this.map.removeSource('source-search');
      }
    },
    addSearchIcon() {
      const img = new Image(80, 80);
      img.onload = () => {
        if (this.map) {
          this.map.addImage('search-icon', img);
        }
      };
      img.src = searchIcon;
    }
  }
};
