import connection from '~/assets/js/connection';

// Станции Ж/Д
const defaultState = {
  stops: [],
}

export const state = () => ({
  ...defaultState
});

export const mutations = {
  SET_STOPS(state, data) {
    state.stops = data
  },
  CLEAR_JD_STORE(state) {
    Object.keys(defaultState).forEach(key => {
      state[key] = defaultState[key]
    })
  }
};

export const actions = {
  async getStations({ commit }) {
    const polyline = require('@mapbox/polyline');
    const point = require('@turf/helpers').point;
    const { data } = await this.$axios.get(
      `${connection.getAddress()}/moscow/jd/stations`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    const decodedData = data.data.map(item => {
      const stop = point(polyline.decode(item.geom)[0].reverse(), {
        name: item.name,
        lineName: item.lineName,
        id: item.id,
        color: item.color,
        mcdNameLatin: item.mcdNameLatin
      });
      stop.id = item.id;
      return stop;
    });
    commit('SET_STOPS', decodedData)
  },
  clearJdStore({ commit }) {
    commit('CLEAR_JD_STORE')
  }
};
