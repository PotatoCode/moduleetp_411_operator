import connection from '~/assets/js/connection';

export const state = () => ({
  data: {
    authorized: null,
    token: null
  },
  redirectParams: null,
});

export const mutations = {
  SET_DATA(state, data) {
    state.data = {
      ...state.data,
      ...data
    };
  },
  setToken(state, token) {
    state.data.token = token
  },
  clearToken(state) {
    state.data.token = null
  },
  setRedirectParams(state, redirectParams) {
    state.redirectParams = redirectParams;
  }
};

export const actions = {
  authenticate({commit}, data) {
    return this.$axios
      .$post(`${connection.getAddress()}/doauth`, {
        username: data.username,
        password: data.password
      })
      .then(res => {
        commit('setToken', res.token);
        localStorage.setItem('token', res.token);
        localStorage.setItem('tokenExpiration', res.expire);
        localStorage.setItem('userEmail', data.username)
      })
  },
  async initAuth({commit, dispatch}) {
    const token = localStorage.getItem('token');
    const expirationDate = localStorage.getItem('tokenExpiration');
    if (new Date().getTime() > Date.parse(expirationDate) || !token) {
      dispatch('logout');
      this.dispatch('users/setAsUser');
      return
    }
    commit('setToken', token);
    commit('SET_DATA', {
      authorized: true,
    });

    if (!window.logoutTimer) {
      const logoutTime = 1200 * 1000;
      window.logoutTimer = setTimeout(() => dispatch('exit'), logoutTime);
      window.addEventListener('mousemove', () => {
        clearTimeout(window.logoutTimer);
        window.logoutTimer = setTimeout(() => dispatch('exit'), logoutTime)
      })
    }

    if (!$nuxt.$store.state.account.summary.login) {
      await this.dispatch('users/getRole');
      this.dispatch('account/getSummary');
      this.dispatch('notifications/getNotificationsCount');
    }
  },
  register({commit}, data) {
    return this.$axios
      .$post(`${connection.getAddress()}/register`, {
        login: data.login,
        username: data.username,
        password: data.password
      })
  },
  logout({commit}) {
    console.log('logout');
    commit('clearToken');
    localStorage.removeItem('token');
    localStorage.removeItem('tokenExpiration')
  },
  exit({dispatch}) {
    dispatch('logout');
    this.dispatch('account/clearAccountStore');
    location.href = '/login';
  }
};
