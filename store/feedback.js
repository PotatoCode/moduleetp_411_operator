import connection from '~/assets/js/connection';

export const state = () => ({
  sending: false,
});

export const mutations = {
  SET_SENDING(state, data) {
    state.sending = data;
  },
};

export const actions = {
  async send( {commit}, data ) {
    commit('SET_SENDING', true);
    this.$axios.post(
      `${connection.getAddress()}/feedback/creation`,
      data.params
    ).then(() => {
      data.success();
      $nuxt.$snotify.success(
        (data.params.type === 'suggestion' ? 'Спасибо за участие в улучшении Портала!' : 'Спасибо за обращение. Ответ будет направлен Вам на указанную электронную почту'),
        {
          position: 'centerTop',
          titleMaxLength: 100
        }
      );
      commit('SET_SENDING', false);
    }).catch(() => {
      $nuxt.$snotify.error(
        'Ошибка',
        {
          position: 'centerTop',
          titleMaxLength: 100
        }
      );
      commit('SET_SENDING', false);
    });
  },
};
