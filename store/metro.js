import connection from '~/assets/js/connection';

// Метро
const defaultState = {
  metro: [],
  entrances: []
}

export const state = () => ({
  ...defaultState
});

export const mutations = {
  SET_METRO(state, data) {
    state.metro = data
  },
  SET_ENTRANCES(state, data) {
    state.entrances = data
  },
  CLEAR_METRO_STORE(state) {
    Object.keys(defaultState).forEach(key => {
      state[key] = defaultState[key]
    })
  }
}

export const actions = {
  async getMetro({ commit }) {
    const polyline = require('@mapbox/polyline');
    const point = require('@turf/helpers').point;
    const { data } = await this.$axios.get(
      `${connection.getAddress()}/moscow/metro/stations`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    )
    const decodedData = data.data.map(item => {
      const stop = point(polyline.decode(item.geom)[0].reverse(), {
        name: item.name,
        lineName: item.lineName,
        id: item.id,
        color: item.color,
      });
      stop.id = item.id;
      return stop;
    });
    commit('SET_METRO', decodedData)
  },

  async getMetroEntrances({ commit }) {
    const polyline = require('@mapbox/polyline');
    const point = require('@turf/helpers').point;
    const { data } = await this.$axios.get(
      `${connection.getAddress()}/moscow/metro/entrances`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    )
    const decodedData = data.data.map(item => {
      const stop = point(polyline.decode(item.geom)[0].reverse(), {
        name: item.name,
        lineName: item.lineName,
        id: item.id,
        color: item.color,
        stationName: item.stationName
      });
      stop.id = item.id;
      return stop;
    });
    commit('SET_ENTRANCES', decodedData)
  },
  clearMetroStore({ commit }) {
    commit('CLEAR_METRO_STORE')
  }
}
