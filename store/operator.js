import connection from '~/assets/js/connection';

export const state = () => ({
  loading: false,
  proposalHistory: [],
});

export const mutations = {
  SET_OPERATOR_LOADING(state, loading) {
    state.loading = loading;
  },
  SET_PROPOSAL_HISTORY(state, data) {
    state.proposalHistory = data;
  }
};

export const actions = {
  async proposalModerate({commit}, guid) {
    commit('SET_OPERATOR_LOADING', true);
    const {data} = await this.$axios.get(
      `${connection.getAddress()}/moscow/proposals/sec/operator/moderate/${guid}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    this.dispatch('proposals/getAllProposals');
    this.dispatch('operator/getProposalHistory', guid);
    this.dispatch('proposals/getProposal', {guid});
    commit('SET_OPERATOR_LOADING', false);
  },
  async proposalDeny({commit}, guid) {
    commit('SET_OPERATOR_LOADING', true);
    const {data} = await this.$axios.get(
      `${connection.getAddress()}/moscow/proposals/sec/operator/deny/${guid}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    this.dispatch('proposals/getAllProposals');
    this.dispatch('operator/getProposalHistory', guid);
    commit('SET_OPERATOR_LOADING', false);
  },
  async proposalReplyDeny({commit}, params) {
    commit('SET_OPERATOR_LOADING', true);
    const {data} = await this.$axios.post(
      `${connection.getAddress()}/moscow/proposals/sec/operator/deny/${params.guid}`,
      {
        textField: params.textField
      },
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    this.dispatch('proposals/getAllProposals');
    this.dispatch('operator/getProposalHistory', params.guid);
    commit('SET_OPERATOR_LOADING', false);
  },
  async proposalWork({commit}, guid) {
    commit('SET_OPERATOR_LOADING', true);
    const {data} = await this.$axios.get(
      `${connection.getAddress()}/moscow/proposals/sec/operator/work/${guid}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    this.dispatch('proposals/getAllProposals');
    this.dispatch('operator/getProposalHistory', guid);
    commit('SET_OPERATOR_LOADING', false);
  },
  async proposalClose({commit}, guid) {
    commit('SET_OPERATOR_LOADING', true);
    const {data} = await this.$axios.get(
      `${connection.getAddress()}/moscow/proposals/sec/operator/close/${guid}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    this.dispatch('proposals/getAllProposals');
    this.dispatch('operator/getProposalHistory', guid);
    commit('SET_OPERATOR_LOADING', false);
  },
  async proposalReplyClose({commit}, params) {
    commit('SET_OPERATOR_LOADING', true);
    const {data} = await this.$axios.post(
      `${connection.getAddress()}/moscow/proposals/sec/operator/close/${params.guid}`,
      {
        textField: params.textField
      },
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    this.dispatch('proposals/getAllProposals');
    this.dispatch('operator/getProposalHistory', params.guid);
    commit('SET_OPERATOR_LOADING', false);
  },
  async proposalReset({commit}, guid) {
    commit('SET_OPERATOR_LOADING', true);
    const {data} = await this.$axios.get(
      `${connection.getAddress()}/moscow/proposals/sec/operator/reset/${guid}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    this.dispatch('proposals/getAllProposals');
    this.dispatch('operator/getProposalHistory', guid);
    commit('SET_OPERATOR_LOADING', false);
  },
  getProposalHistory({commit}, guid) {
    commit('SET_OPERATOR_LOADING', true);
    return this.$axios.get(
      `${connection.getAddress()}/moscow/proposals/sec/operator/proposals/history/${guid}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    ).then(({data}) => {
      commit('SET_PROPOSAL_HISTORY', data.data);
      commit('SET_OPERATOR_LOADING', false);
    })
      .catch(e => {
        console.info(e);
      })
  }
};
