import connection from '~/assets/js/connection';

// Опросы
const defaultState = {
  allPolls: [],
  pollsList: [],
  pollsRemaining: 0,
  pollsRoutes: [],
  pollRoute: [],
  pollStops: [],
  discussions: [],
  discussionsSending: false,
  discussionsLoading: false,
};

export const state = () => ({
  ...defaultState
});

export const mutations = {
  SET_POLL_ROUTE(state, data) {
    state.pollRoute = data
  },
  SET_POLL_STOPS(state, data) {
    state.pollStops = data
  },
  SET_POLLS_ROUTES(state, data) {
    state.pollsRoutes = data
  },
  SET_POLLS_LIST(state, data) {
    state.pollsList = data
  },
  ADD_POLLS(state, data) {
    state.pollsList = [...state.pollsList, ...data]
  },
  CLEAR_POLLS_STORE(state) {
    Object.keys(defaultState).forEach(key => {
      state[key] = defaultState[key]
    })
  },
  SET_POLL_DISCUSSIONS(state, data) {
    state.discussions = data;
  },
  SET_DISCUSSIONS_SENDING(state, data) {
    state.discussionsSending = data;
  },
  SET_DISCUSSIONS_LOADING(state, data) {
    state.discussionsLoading = data;
  },
  SET_POLLS_REMAINING(state, data) {
    state.pollsRemaining = data
  },
  SET_ALL_POLLS(state, data) {
    state.allPolls = data
  }
};

export const actions = {
  async getAllPolls({ commit }) {
    const { data } = await this.$axios.get(
      `${connection.getAddress()}/moscow/polls/list`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    commit('SET_ALL_POLLS', data.data);
  },
  async getPollsRoutes({commit}) {
    const {data} = await this.$axios.get(
      `${connection.getAddress()}/moscow/polls/ngpt/routes/list`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    commit('SET_POLLS_ROUTES', data.data)
  },
  async getPollsList({commit}, params) {
    let paramsStr = '';
    if (params) {
      for (let i in params) {
        if (i !== 'reset') {
          paramsStr += `${i}=${typeof params[i] === 'object' ? JSON.stringify(params[i]) : params[i]}&`;
        }
      }
    }
    const {data} = await this.$axios.get(
      `${connection.getAddress()}/moscow/polls/list${paramsStr.length !== 0 ? `?${paramsStr}` : ''}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    if (data.remaining !== undefined) {
      commit('SET_POLLS_REMAINING', data.remaining)
    }
    if (!params || (params && params.reset)) {
      commit('SET_POLLS_LIST', data.data)
    } else {
      commit('ADD_POLLS', data.data)
    }
  },
  saveVote({actions}, vote) {
    this.$axios.post(
      `${connection.getAddress()}/moscow/polls/vote`,
      vote,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        },
      }
    ).then(() => {
      this.dispatch('polls/getAllPolls');
      $nuxt.$snotify.success(
        'Ваш голос успешно учтен',
        {
          position: 'centerTop',
          titleMaxLength: 100
        }
      );
    }).catch(() => {
      $nuxt.$snotify.error(
        'Ошибка',
        {
          position: 'centerTop',
          titleMaxLength: 100
        }
      );
    });
  },
  async getPollStopsById({commit}, routeID) {
    const {data} = await this.$axios.get(
      `${connection.getAddress()}/moscow/polls/ngpt/stops/byRouteID/${routeID}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    commit('SET_POLL_STOPS', data.features)
  },
  async getPollRouteById({commit}, routeID) {
    const {data} = await this.$axios.get(
      `${connection.getAddress()}/moscow/polls/ngpt/routes/byRouteID/${routeID}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    commit('SET_POLL_ROUTE', data.features)
  },
  clearPollsStore({commit}) {
    commit('CLEAR_POLLS_STORE')
  },
  async getPolllDiscussions({commit}, data) {
    // data.id = '0d36b5a4-a0cc-48e4-a4a9-75f31d92bea2';
    if (data.loading) {
      commit('SET_DISCUSSIONS_LOADING', true);
    }
    this.$axios.get(
      `${connection.getAddress()}/moscow/polls/discussions/byPollGUID//${data.id}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        },
      }
    ).then(({data}) => {
      commit('SET_POLL_DISCUSSIONS', data.data);
      commit('SET_DISCUSSIONS_LOADING', false);
    }).catch(() => {
      console.error('getPollDiscussions error');
    });
  },
  async sendPollDiscussionsComment({commit}, data) {
    commit('SET_DISCUSSIONS_SENDING', true);
    let url = `${connection.getAddress()}/moscow/polls/discussions/post/${data.guid}/${data.parentGid}`;
    this.$axios.post(
      url,
      {
        textField: data.textField,
      },
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        },
      }
    ).then(() => {
      this.dispatch('polls/getPolllDiscussions', {
        id: data.guid
      });
      commit('SET_DISCUSSIONS_SENDING', false);
    }).catch(() => {
      console.error('sendPollDiscussionsComment error');
    });
  }
}
